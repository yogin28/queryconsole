<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>

    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link href="${jstlCss}" rel="stylesheet"/>

</head>
<body>

<div>
    <form action="/saveQuery" method="post" modelAttribute="queryData" class="container">
        ${result}
        Query:<br>
        <textarea name="query" cols="30" rows="3"></textarea>
        <br><br>
        Email:<br>
        <textarea name="listOfEmail" id="" cols="30" rows="3"></textarea>
        <br><br>
        Hour Of Day:
        <select name="hourOfDay">
            <option value="0">12:00 am</option>
            <option value="1">1:00 am</option>
            <option value="2">2:00 am</option>
            <option value="3">3:00 am</option>
            <option value="4">4:00 am</option>
            <option value="5">5:00 am</option>
            <option value="6">6:00 am</option>
            <option value="7">7:00 am</option>
            <option value="8">8:00 am</option>
            <option value="9">9:00 am</option>
            <option value="10">10:00 am</option>
            <option value="11">11:00 am</option>
            <option value="12">12:00 pm</option>
            <option value="13">1:00 pm</option>
            <option value="14">2:00 pm</option>
            <option value="15">3:00 pm</option>
            <option value="16">4:00 pm</option>
            <option value="17">5:00 pm</option>
            <option value="18">6:00 pm</option>
            <option value="19">7:00 pm</option>
            <option value="20">8:00 pm</option>
            <option value="21">9:00 pm</option>
            <option value="22">10:00 pm</option>
            <option value="23">11:00 pm</option>
        </select>
        <br><br>
        <input type="submit" value="Submit">
    </form>

</div>

<script type="text/javascript"
        src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>
