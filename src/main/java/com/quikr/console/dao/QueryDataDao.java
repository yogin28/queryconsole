package com.quikr.console.dao;

import com.quikr.console.model.QueryData;

import java.util.List;
import java.util.Map;

public interface QueryDataDao {

    void saveQueryData(QueryData queryData);

    List<QueryData> findByHourOfDay(Integer hourOfDay);

    List<Map<String, Object>> executeCustomQuery(String query);

}
