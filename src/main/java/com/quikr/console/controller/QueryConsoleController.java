package com.quikr.console.controller;


import com.quikr.console.model.QueryData;
import com.quikr.console.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@Controller
public class QueryConsoleController {

    @Autowired
    private QueryService queryService;

    @GetMapping("/")
    public String renderConsolePage() {
        return "welcome";
    }

    @PostMapping("/saveQuery")
    public String saveConsoleForm(@ModelAttribute QueryData queryData, Map map) {
        String resutl = "";
        if (queryData != null && !queryData.getQuery().isEmpty()&& !queryData.getListOfEmail().isEmpty() && queryData.getHourOfDay() != null ) {
            resutl = queryService.saveQueryData(queryData);
        }else{
            resutl = "All fields are mandatory.";
        }
        map.put("result",resutl);
        return "welcome";
    }
}