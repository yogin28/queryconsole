package com.quikr.console.config;

import com.quikr.console.helper.EmailStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Component("emailSender")
public class MailSender {

    @Autowired
    JavaMailSender javaMailSender;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public EmailStatus sendPlainTextMail(String[] to, String subject, String text) {
        return sendM(to, subject, text, false);
    }

    public EmailStatus sendHtmlMail(String[] to, String subject, String htmlBody) {
        return sendM(to, subject, htmlBody, true);
    }

    private EmailStatus sendM(String[] to, String subject, String text, Boolean isHtml) {
        try {
            MimeMessage mail = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text, isHtml);
            javaMailSender.send(mail);
            logger.info("Send email '{}' to: {}", subject, to);
            return new EmailStatus(to, subject, text).success();
        } catch (Exception e) {
            logger.error(String.format("Problem with sending email to: {}, error message: {}", to, e.getMessage()));
            return new EmailStatus(to, subject, text).error(e.getMessage());
        }
    }

    public EmailStatus sendMailWithCSV(String csvData, String[] to, String subject) throws Exception {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setSubject(subject);
            helper.setText("test text");
            helper.setTo(to);
            helper.setFrom("yogin2017@gmail.com");
            helper.addAttachment("Query-Data.csv",
                    new ByteArrayResource(csvData.getBytes(), "text/csv"));
            javaMailSender.send(message);
            logger.info("Send email '{}' to: {}", subject, to);
            return new EmailStatus(to, subject, csvData).success();
        } catch (AddressException e) {
            logger.error(String.format("Problem with sending email to: {}, error message: {}", to, e.getMessage()));
            throw new AddressException("[sendEmail]: Incorrect email address" + e.getMessage());

        } catch (MessagingException e) {
            logger.error(String.format("Problem with sending email to: {}, error message: {}", to, e.getMessage()));
            throw new MessagingException("[sendEmail]: Unable to send email" + e.getMessage());

        } catch (Exception e) {
            logger.error(String.format("Problem with sending email to: {}, error message: {}", to, e.getMessage()));
            throw new Exception("[sendEmail]: Error in method " + e.getMessage());
        }

    }
}
