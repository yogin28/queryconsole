package com.quikr.console.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;


public class CustomExceptionHandler implements AsyncUncaughtExceptionHandler {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void handleUncaughtException(
            Throwable throwable, Method method, Object... obj) {

        logger.error("Exception message - " + throwable.getMessage());
        logger.error("Method name - " + method.getName());
        for (Object param : obj) {
            logger.error("Parameter value - " + param);
        }
    }
}
