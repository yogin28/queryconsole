package com.quikr.console.helper;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CSVBuilder {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    public String convertDataToCSV(List<Map<String, Object>> mapList) {
        StringBuffer header = new StringBuffer();
        StringBuffer data = new StringBuffer();
        boolean flag = true;
        for (Map<String, Object> map : mapList) {
            if (flag) {
                for (String name : map.keySet()) {
                    header.append(name.replaceAll(COMMA_DELIMITER, " "));
                    header.append(COMMA_DELIMITER);
                }
            }
            for (Object value : map.values()) {
                data.append(value.toString().replaceAll(COMMA_DELIMITER, " "));
                data.append(COMMA_DELIMITER);
            }
            data.append(NEW_LINE_SEPARATOR);
            flag = false;
        }
        header.append(NEW_LINE_SEPARATOR);

        return header + "" + data;
    }
}
