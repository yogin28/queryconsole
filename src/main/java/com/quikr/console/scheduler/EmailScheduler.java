package com.quikr.console.scheduler;

import com.quikr.console.helper.Utils;
import com.quikr.console.model.QueryData;
import com.quikr.console.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class EmailScheduler {

    @Autowired
    Utils utils;

    @Autowired
    QueryService queryService;

    // Every Minutes
    // @Scheduled(cron = "0 0/1 * 1/1 * ?")
    // Every Hour
    @Scheduled(cron = "0 0 0/1 1/1 * ?")
    public void scheduleTastForEveryHourOfDay() {
        int hour = utils.getHourOfCurrentTimeStamp(new Date());
        System.out.println(hour);
        List<QueryData> queryDataList = queryService.fetchAllRowsBasedHour(hour);
        queryService.executeCustomQuery(queryDataList);

    }

}
