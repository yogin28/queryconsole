package com.quikr.console.service;

import com.quikr.console.model.QueryData;

import java.util.List;

public interface QueryService {

    String saveQueryData(QueryData queryData);

    List<QueryData> fetchAllRowsBasedHour(int hourOfDay);

    void executeCustomQuery(List<QueryData> queryDataList);
}
