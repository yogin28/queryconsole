package com.quikr.console.serviceImpl;

import com.quikr.console.dao.QueryDataDao;
import com.quikr.console.model.QueryData;
import com.quikr.console.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QueryServiceImpl implements QueryService {

    @Autowired
    QueryDataDao queryDataDao;

    @Autowired
    EmailAsyncService asynchronousService;

    @Override
    public String saveQueryData(QueryData queryData) {
        queryDataDao.saveQueryData(queryData);
        return "Successfully Inserted into DB.";
    }

    @Override
    public List<QueryData> fetchAllRowsBasedHour(int hourOfDay) {
        return queryDataDao.findByHourOfDay(hourOfDay);
    }

    @Override
    public void executeCustomQuery(List<QueryData> queryDataList) {
        queryDataList.forEach(queryData ->
                asynchronousService.asyncEmailSender(queryDataDao.executeCustomQuery(queryData.getQuery()), queryData.getListOfEmail())
        );
    }


}