package com.quikr.console.serviceImpl;

import com.quikr.console.config.MailSender;
import com.quikr.console.helper.CSVBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EmailAsyncService {

    @Autowired
    @Qualifier("emailSender")
    public MailSender mailSender;

    @Autowired
    CSVBuilder csvBuilder;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Async
    public void asyncEmailSender(List<Map<String, Object>> mapList, String emails) {

        if (mapList != null && !emails.isEmpty()) {
            try {
                String csvData = csvBuilder.convertDataToCSV(mapList);
                String[] to = emails.split(",");
                String subject = "Query Data.";
                mailSender.sendMailWithCSV(csvData, to, subject);
            } catch (Exception e) {
                logger.error(String.format("Problem with sending email error message: {}", e.getMessage()));
            }
        }
    }
}
