package com.quikr.console.daoImpl;

import com.quikr.console.dao.QueryDataDao;
import com.quikr.console.model.QueryData;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class QueryDataDaoImpl implements QueryDataDao {

    @Autowired
    SessionFactory sessionFactory;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void saveQueryData(QueryData queryData) {
        Session session = sessionFactory.getCurrentSession();
        session.save(queryData);
    }

    @Override
    public List<QueryData> findByHourOfDay(Integer hourOfDay) {

        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<QueryData> query = builder.createQuery(QueryData.class);
        Root<QueryData> root = query.from(QueryData.class);
        query.select(root).where(builder.equal(root.get("hourOfDay"), hourOfDay));
        Query q = session.createQuery(query);
        List<QueryData> queryData = q.getResultList();
        return queryData;
    }

    @Override
    public List<Map<String, Object>> executeCustomQuery(String query) {
        if (!query.isEmpty()) {
            try {
                Session session = sessionFactory.getCurrentSession();
                NativeQuery<Map<String, Object>> q = session.createNativeQuery(query);
                q.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
                List<Map<String, Object>> resutl = q.list();
                return resutl;
            } catch (Exception e){
                logger.error("SQL syntax error query {} error message {}", query, e.getMessage());
            }

        }
        return null;

    }
}
